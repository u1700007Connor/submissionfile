# README #

This README is a guide to install the Ghidra submission files

# CODEMETRICS #
Take the code mtrics java file and copy it into the Ghidra script directory. By default this should be 
\ghidra_9.1_PUBLIC\Ghidra\Features\Base\ghidra_scripts
# GHIDRAGRAPHFORMAT #
Take the other files which should be within one file by time of uploading
Within ghidra go to File>Install Extensions... and click the plus sign.
Navigate to the location of the file and open it.

# KNOWN ERRORS #
ControlFlowChart may cause an incorrect version error despite being on the same version. Restart Ghidra, if error continues redownload file.
If issue continues to persist, re-zip files together and re-follow installation instructions. On continueation, contact email below.
ControlFlowChart may have image error despite using a default Ghidra image, non-fatal.

# UNKNOWN ERRORS #

ON an error unknown, please contact
connorberry99@yahoo.com
to make me aware of it.