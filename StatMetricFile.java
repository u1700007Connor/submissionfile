//
//@author Connor Berry
//@category Stat Metrics
//@keybinding
//@menupath
//@toolbar

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.filechooser.FileNameExtensionFilter;

import ghidra.app.script.GhidraScript;
import ghidra.program.model.listing.Function;
import ghidra.program.model.listing.FunctionIterator;
import ghidra.program.model.listing.FunctionManager;
import ghidra.program.model.listing.Program;
import ghidra.program.util.CyclomaticComplexity;
import ghidra.util.task.TaskMonitor;

public class StatMetricFile extends GhidraScript {
	
	//need dummy to avoid nullpointerexception
	TaskMonitor monitor1 = getMonitor().DUMMY;
	
	
	@Override
	protected void run() throws Exception {
		File file = null;
		
		//Simple solution to file checking, error/misuse causes program to cancel, works out either way.
		JFrame frame = new JFrame();
		 
		JFileChooser chooser = new JFileChooser();
		chooser.setDialogTitle("Specify a file to save");   
		int userSelection = chooser.showSaveDialog(frame);
		//only text files, nothing else.
		chooser.addChoosableFileFilter(new FileNameExtensionFilter("text file", "txt"));
		if (userSelection == JFileChooser.APPROVE_OPTION) {
		    file = chooser.getSelectedFile();
		    
		    System.out.println("Save as file: " + file.getAbsolutePath());
		}
		BufferedWriter writing = new BufferedWriter(new FileWriter(file));
		//program gets currently loaded program in codebrowser
		Program program = getCurrentProgram();
		//hold and go are the program addresses of the first and last respectively
		String hold = program.getMinAddress().toString();
		String go = program.getMaxAddress().toString();
		//Pound symbol and name for file for organization
		writing.write("#Metrics File");
		writing.newLine();
		writing.newLine();
		writing.write("Region ALL");
		writing.newLine();
		writing.write("The first address is "+hold);
		writing.newLine();
		writing.write("The last address is "+go);
		writing.newLine();
		
		//Function manager and iterator needed for calls later
		FunctionManager functionManager = program.getFunctionManager();
		FunctionIterator iter = functionManager.getFunctionsNoStubs(true);
		
		//Address set view gets all code that's executed
		long ins = program.getListing().getNumInstructions();

		writing.write("The overall INS is "+ins);
		writing.newLine();
		
		writing.write("...");
		writing.newLine();
		writing.write("#");
		writing.newLine();
		for(Function func : iter) {
			//may want to comment out Iterator for speed & if only concerned for program overall.
			func.getBody().getNumAddresses();
			int place1 = 0;
			int place2 = 0;
			try {
			place1 = func.getCallingFunctions(monitor1).size(); 
			place2 = func.getCalledFunctions(monitor1).size();
			} catch(NullPointerException e) {
				//Nonissue; continue
			}
			//may want to comment out CC for speed
			CyclomaticComplexity Cyclone = new CyclomaticComplexity();
			int space = 0;
			try {
				space = Cyclone.calculateCyclomaticComplexity(func, monitor1);
			} catch(NullPointerException e) {
				//again, nonissue, check official Ghidra documentation for confirmation
			}
			//remember to comment out relevant writes if commenting out previous code.
			func.getBody().getMinAddress();
			
			writing.write("Beggining address "+func.getBody().getMinAddress());
			writing.newLine();
			writing.write("Final Address "+func.getBody().getMaxAddress());
			writing.newLine();
			writing.write("Calling Functions "+place1);
			writing.newLine();
			writing.write("Called Functions "+place2);
			writing.newLine();
			writing.write("INS "+func.getBody().getNumAddresses());
			writing.newLine();
			writing.write("CC "+space);
			writing.newLine();
			writing.write("...");
			writing.newLine();
			writing.write("#");
			writing.newLine();
		}
		writing.newLine();
		writing.write("#End");
		writing.close();
	}
}
